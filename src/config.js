// 配置项

const host = 'http://localhost:5757'
    // const host = 'https://a5l6wgcl.qcloud.la'

const config = {
    host,
    loginUrl: `${host}/weapp/login`,
    userUrl: `${host}/weapp/user`
}
export default config