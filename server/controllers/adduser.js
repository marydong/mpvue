const { mysql } = require('../qcloud')

module.exports = async(ctx) => {
    const { open_id, user_info } = ctx.request.body
    if (open_id && user_info) {
        const findRes = await mysql('csessioninfo').select().where('open_id', open_id)
        if (findRes.length) {
            return
        }
        try {
            await mysql('csessioninfo').insert({
                open_id,
                user_info
            })
        } catch (e) {
            ctx.state = {
                code: -1,
                data: {
                    msg: '新增用户失败:' + e.sqlMessage
                }
            }
        }
    }
}